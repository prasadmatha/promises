/*1. Create a promise. Have it resolve with a value of Promise Resolved! in resolve after a delay of 1000ms, using setTimeout. 
Print the contents of the promise after it has been resolved by passing console.log to .then.*/

const promise = new Promise(function (resolve,reject){
    setTimeout(function(){
        resolve("Promise Resolved!")
    },1000)
})

promise.
then(message => console.log(message))
.catch(error => console.log(error))

