/*3. Create another promise. Now have it reject with a value of Rejected Promise! without using setTimeout. 
Print the contents of the promise after it has been rejected by passing console.log to .catch and also use .finally to log the message Promise Settled!.*/


const promise = new Promise((resolve,reject)=>{
    reject("Rejected Promise!")
})

promise.then(message => console.log(message))
.catch(error => console.log(error))
.finally(()=>console.log("Promise Settled!"))